# Builder stage
FROM rust:latest as builder

WORKDIR /usr/src/vecdb
COPY . .
# Compile the project in release mode. The binary will be at target/release/webapp
RUN cargo build --release

# Final stage
FROM debian:latest

ARG QDRANT_API_KEY
ENV QDRANT_API_KEY=${QDRANT_API_KEY}

# Install runtime dependencies and clean up in a single RUN statement to reduce image size
RUN apt-get update && apt-get install -y \
    libfontconfig1 \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# Copy the compiled binary from the builder stage
COPY --from=builder /usr/src/vecdb/target/release/vecdb /usr/local/bin/vecdb

# Command to run the application
CMD ["vecdb"]
