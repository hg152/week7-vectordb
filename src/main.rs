use std::collections::HashMap;
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{SearchPoints, CreateCollection, Distance, PointStruct, vectors_config::Config, VectorParams, VectorsConfig};
use env_logger;
use log::{info, log};
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::Value;
use plotters::prelude::*;

#[derive(Serialize, Deserialize)]
struct TextData {
    id: u64,
    vector: Vec<f32>,
    payload: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize)]
struct InsertRequest{
    collection_name: String,
    data: Vec<TextData>,
}

#[derive(Serialize, Deserialize)]
struct QueryRequest {
    collection_name: String, // Example collection name
    vector: Vec<f32>, // Example query vector
    limit: u64, // Example limit
}

#[derive(Serialize, Deserialize)]
struct CreateCollectionRequest{
    collection_name: String,
    vector_size: u64,
}

// write a function that visializes the score of the search result(vector of floats)
fn visualize_scores(scores: Vec<f32>) -> Result<(), Box<dyn std::error::Error>> {
    let root_area = BitMapBackend::new("scores.png", (640, 480)).into_drawing_area();
    root_area.fill(&WHITE)?;

    // Initialize the frequency count for each bin with zeros
    let mut bins = vec![0; 10];

    // Populate the bins based on scores
    for score in scores {
        let index = (score * 10.0).min(9.0) as usize; // Ensure index is within bounds [0, 9]
        bins[index] += 1;
    }

    // Creating the chart
    let mut chart = ChartBuilder::on(&root_area)
        .caption("Score Distribution", ("sans-serif", 40).into_font())
        .margin(5)
        .x_label_area_size(50)
        .y_label_area_size(40)
        .build_ranged(0f32..10f32, 0..*bins.iter().max().unwrap_or(&0) + 1)?;

    chart.configure_mesh().x_labels(10).y_labels(10).draw()?;

    // Drawing the bars for the histogram
    chart.draw_series(
        bins.into_iter().enumerate().map(|(index, freq)| {
            let x0 = index as f32;
            let x1 = x0 + 1.0;
            Rectangle::new([(x0, 0), (x1, freq)], RED.filled())
        }),
    )?;

    // Finalize the drawing
    root_area.present()?;

    println!("Histogram saved as 'scores_histogram.png'");
    Ok(())
}

async fn make_client() -> Result<QdrantClient, Box<dyn std::error::Error>> {
    info!("API KEY: {}", std::env::var("QDRANT_API_KEY").unwrap_or_default().to_string());

    let client = QdrantClient::from_url("https://2ba88247-bc7d-4b8f-8950-c47f609f8ca7.us-east-1-0.aws.cloud.qdrant.io:6334")
        // using an env variable for the API KEY for example
        .with_api_key(std::env::var("QDRANT_API_KEY").unwrap_or_default().to_string())
        .build()?;
    Ok(client)
}

#[get("/query")]
async fn query_vectors(req: web::Json<QueryRequest>) -> impl Responder {
    let client = make_client().await.unwrap();
    info!("Client created successfully");

    let data = req.into_inner();

    let search_result = client
    .search_points(&SearchPoints {
        collection_name: data.collection_name,
        vector: data.vector,
        limit: data.limit,
        with_payload: Some(true.into()),
        ..Default::default()
    })
    .await;

    let mut scores = Vec::new();

    // Execute the search query
    match search_result {
        Ok(results) => {
            info!("Client got scores");

            for r in results.result {
                scores.push(r.score);
            }

            // Visualize the scores and send through http body
            match visualize_scores(scores.clone()) {
                Ok(_) => {
                    let body = std::fs::read("scores.png").unwrap();

                    //delete the file
                    std::fs::remove_file("scores.png").unwrap();
                    HttpResponse::Ok().content_type("image/png").body(body)
                },
                Err(e) => HttpResponse::InternalServerError().body(e.to_string()),
            }
        },
        Err(e) => HttpResponse::InternalServerError().body(format!("Query failed: {}", e)),
    }
}



#[post("/insert")]
async fn insert_data(req_raw: web::Json<InsertRequest>) -> impl Responder {
    let client = make_client().await.unwrap();
    info!("Client created successfully");

    let req = req_raw.into_inner();
    let collection_name = req.collection_name;
    let data = req.data;
    let mut points = Vec::new();
    for datum in data {
        let point = PointStruct::new(datum.id, datum.vector, json!(datum.payload).try_into().unwrap()).try_into().unwrap();
        points.push(point);
    }
    let result = client
        .upsert_points(&collection_name,  None, points, None)
        .await;
    match result {
        Ok(_) => HttpResponse::Ok().body("Data inserted successfully"),
        Err(e) => HttpResponse::InternalServerError().body(format!("Error inserting data: {}", e)),
    }
}



#[post("/create")]
async fn create_collection(req_raw: web::Json<CreateCollectionRequest>) -> impl Responder {
    let req = req_raw.into_inner();
    let client = make_client().await.unwrap();
    info!("Client created successfully");
    let result = client
        .create_collection(&CreateCollection {
            collection_name: req.collection_name,
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: req.vector_size,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await;

    match result {
        Ok(_) => HttpResponse::Ok().body("Collection created successfully"),
        Err(e) => HttpResponse::InternalServerError().body(format!("Error creating collection: {}", e)),
    }
}


#[actix_web::main]async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug"); // Adjust log level as needed
    env_logger::init();
    info!("starting server");

    HttpServer::new(|| {
        App::new()
            .service(create_collection)
            .service(insert_data)
            .service(query_vectors)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}

