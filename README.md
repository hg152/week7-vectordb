# Vector DB Project

[![Pipeline Status](https://gitlab.com/hg152/week7-vectordb/badges/master/pipeline.svg)](https://gitlab.com/hg152/week7-vectordb/-/pipelines)

This project is a Rust-based web application that provides connection to a qdrant vector database instance hosted through qdrant cloud. It supports collection creation, insertion. It can also be used to query to collection and generate a histogram of scores of the instances stored in the collection with respect to the query vector.

## Tools and Technologies

- **Rust**: A systems programming language that guarantees memory safety and offers great performance.
- **Actix-web**: A powerful, pragmatic, and extremely fast web framework for Rust.
- **Docker**: A set of platform-as-a-service products that use OS-level virtualization to deliver software in packages called containers.
- **AWS Elastic Container Registry (ECR)**: A fully managed Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images.
- **AWS App Runner**: A fully managed service that makes it easy for developers to quickly deploy containerized web applications and APIs, at scale and with no prior infrastructure experience required.

## Before Running

To run this application, you'll need to set up a cluster through Qdrant cloud, which is a managed Vector Database service. You'll also need to generate an API key and take notes of the API key and end point.

![Alt text](https://i.imgur.com/1ZW8yta.png)

## Running Locally

To run this application locally, you'll need to have Rust, Docker, and Cargo installed on your system.

1. **Clone the Repository**

    ```
    git clone https://your-repository-url.git
    cd your-project-directory
    ```

2. **Load the API endpoint and key**
    
    Modify main.rs at line 81 to include your API Endpoint's URL. Then, load the API key as an environmental variable using the following command.
    ```
    $Env:QDRANT_API_KEY = "<API_KEY>"
    ```

3. **Build the Docker Image**

    Navigate to the root of your project directory and run:

    ```
    docker build --build-arg QDRANT_API_KEY=$QDRANT_API_KEY -t vcb_image .
    ```

4. **Run the Container**

    This command starts the application and exposes it on port 8080 of your localhost:

    ```
    ocker run -p 8080:8080 vcb_image
    ```

## Running On AWS

This project can be deployed on AWS through AWS ECR and AWS AppRunner. AWS ECR is a container registry that is used to store built docker image in our case. AWS AppRunner is a convinient tool of deploying docker images in the AWS environment. A live verision of the script is hosted at https://wt4tnxybps.us-east-1.awsapprunner.com. 

1. **Set Up AWS Resources**

    You'll need to first create a AWS ECR Repo and make note of its endpoint:

    ![Alt text](https://i.imgur.com/YnHBcHa.png)

    Then, you'll need to set up a AWS AppRuner instance to deploy the corresponding ECR repo:

    ![Alt text](https://i.imgur.com/UO8cY1h.png)


2. **Clone the Repository**

    You need to first make a fork of this project in your own gitlab environment.

3. **Load the API endpoint**
    
    Modify main.rs at line 81 to include your API Endpoint's URL. Then, load the API key as an environmental variable using the following command.

4. **Set Up Gitlab CI/CD variables**

    In your version of the gitlab repo, set up the following CI/CD variables: AWS_ACCESS_KEY_ID, AWS_DEFAULT_REGION, AWS_SECRET_ACCESS_KEY, QDRANT_API_KEY, QDRANT_ENDPOINT.

    ![Alt text](https://i.imgur.com/VECG5xe.png)

5. **Modify the GitLab CI/CD yaml file**

    Modify line 20 and line 21 of the gitlab CI/CD yaml file to include your ECR endpoint:

    ```
    - docker tag $IMAGE_TAG <ECR_ENDPOINT>:latest
    - docker push <ECR_ENDPOINT>:latest:latest
    ```

6. **Push to Master Branch**

    Make a commit to your repo and push to the master branch to run the CI/CD pipeline. It should build a Docker Image and then push it to ECR. Following that, the AWS AppRunner will automatically grab the latest image and deploy it.

## Testing the Application

Once a version of this application is running, we can then test the functionality of the application.

### Create an Collection

This application can be used to create a collection in the connected Qdrant Cluster. To use this functionality, we need to create a HTTP Post request to `<endpoint of the application>/create` with the following body attached: 
```
{
    collection_name: String,
    vector_size: u64,
}
```
This specifies the name of the collection being created and the size of the vectors in the collection.

![Alt text](https://i.imgur.com/nDvEr3G.png)

### Insert into a Collection

This application can be used to insert into a collection. To use this functionality, we need to create a HTTP Post request to `<endpoint of the application>/insert` with the following body attached: 
```
{
    collection_name: String,
    data: [
        {
            id: u64,
            vector: Vec<f32>,
            payload: HashMap<String, Value>
        }
        ...
    ]
}
```
which specifies the name of the collection to query and data to be inserted into the collection.

![Alt text](https://i.imgur.com/xpdEJnI.png)

### Query a Collection

This application can be used to query a collection. The application will take the scores of the vectors in the collection with respect to the query vector and generate a histogram. To use this functionality, we need to create a HTTP Get request to `<endpoint of the application>/query` with the following body attached: 
```
{
    collection_name: String, 
    vector: Vec<f32>, 
    limit: u64, 
}
```
which specifies the name of the collection to query, the reference vector, and the maximum number of scores to consider.

![Alt text](https://i.imgur.com/RKKwXVG.png)
